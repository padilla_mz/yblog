

SITE_NAME = "holayadira.com"
SITE_TITLE = "Hola Yadira"
SITE_URL = "http://holayadira.com/"
META_DESCRIPTION = "Sitio informativo"
META_KEYWORDS = ""
EMAIL = "vota@holayadira.com"
EMAIL2 = "contacto@holayadira.com"

#will_paginate
# POSTS_PER_PAGE = 12

#Locale
TWITTER_LOCALE = "en" #default "en"
FACEBOOK_LOCALE = "en_US" #default "en_US"
GOOGLE_LOCALE =  "es-419" #default "en", espanol latinoamerica "es-419"

#ANALYTICS
GOOGLE_ANALYTICS_ID = "UA-48771067-5" #your tracking id

#SOCIAL

# DISQUS_SHORTNAME = ""
TWITTER_USERNAME1 = "holaarandas"
TWITTER_USERNAME2 = "hola_yadira"
FACEBOOK_URL2 = "https://www.facebook.com/pages/Yadira-Guzm%C3%A1n-Alvizo/830353097020960"
YOUTUBE = "https://www.youtube.com/channel/UCL61a9rA7ey_V460Yd7RYqg"

