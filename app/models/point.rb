class Point < ActiveRecord::Base

	validates :lat, :long, presence: true, uniqueness: true
	validates :title, presence: true

end
