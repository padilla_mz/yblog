class PollsController < ApplicationController
  before_action :set_poll, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  respond_to :html, :json
  

  def index
    @body_class = "admin"   

    @polls = Poll.recent
    respond_with(@polls)
  end

  def show
    @body_class = "admin"   

    respond_with(@poll)
  end

  def new
    @body_class = "admin"   

    @poll = Poll.new
    respond_with(@poll)
  end

  def edit
    @body_class = "admin"   
    
  end

  def create
    @poll = Poll.new(poll_params)
    @poll.save
    respond_with(@poll)
  end

  def update
    @poll.update(poll_params)
    respond_with(@poll)
  end

  def destroy
    @poll.destroy
    respond_with(@poll)
  end

  private
    def set_poll
      @poll = Poll.find(params[:id])
    end

    def poll_params
      params.require(:poll).permit(:point)
    end
end
