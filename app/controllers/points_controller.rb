class PointsController < ApplicationController
  before_action :set_point, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  respond_to :html, :json
  
  def index
    @body_class = 'admin'
    @points = Point.all
    respond_with(@points)
  end

  def show
    @body_class = 'admin'
    respond_with(@point)
  end

  def new
    @body_class = 'admin'

    @point = Point.new
    respond_with(@point)
  end

  def edit
    @body_class = 'admin'

  end

  def create
    @point = Point.new(point_params)
    @point.save
    respond_with(@point)
  end

  def update
    @point.update(point_params)
    respond_with(@point)
  end

  def destroy
    @point.destroy
    respond_with(@point)
  end

  private
    def set_point
      @point = Point.find(params[:id])
    end

    def point_params
      params.require(:point).permit(:lat, :long, :title, :desc)
    end
end
