class AdminController < ApplicationController
	before_action :authenticate_user!

  def index
		@body_class = "admin"
		@puntos = Point.count
		@encuestas = Poll.count  	
  end

  def points
		@body_class = "admin"  	
  	
    @points = Point.all	
  end
end
