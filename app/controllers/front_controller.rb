class FrontController < ApplicationController
	before_action :list_markers

  require 'open-uri'
  # require "json"

  def index

    # @t_followers = TWITTER.followers("hola_yadira").count + TWITTER.followers("holaarandas").count

    # tweets_hola_yadira = TWITTER.search("to:hola_yadira", result_type: "all").take(10)
    # tweets_holaarandas = TWITTER.search("to:holaarandas", result_type: "all").take(10)

    # @recent_tweets = TWITTER.search("to:hola_yadira", result_type: "recent").take(20)

  	@list_markers = list_markers
    @marker_info = Point.all
    @poll = Poll.first
  end


  def contadores
    @body_class = "contadores" 
    # file1 = open("https://twitter.com/<%= TWITTER_USERNAME1 %>")
    # @doc = Nokogiri::HTML(file) do |config|
    #   config.nonet.noerror
    # end

    holaarandas = Nokogiri::HTML(open("https://twitter.com/#{TWITTER_USERNAME1}"))
    hola_yadira = Nokogiri::HTML(open("https://twitter.com/#{TWITTER_USERNAME2}"))
    instagram_hola_yadira = Nokogiri::HTML(open("https://instagram.com/hola_yadira"))

    @amigos_holaarandas = holaarandas.at_css("#page-container > div.ProfileCanopy.ProfileCanopy--withNav > div > div.ProfileCanopy-navBar > div > div > div.Grid-cell.u-size2of3.u-lg-size3of4 > div > div > ul > li.ProfileNav-item.ProfileNav-item--followers > a > span.ProfileNav-value")
    @amigos_hola_yadira = hola_yadira.at_css("#page-container > div.ProfileCanopy.ProfileCanopy--withNav > div > div.ProfileCanopy-navBar > div > div > div.Grid-cell.u-size2of3.u-lg-size3of4 > div > div > ul > li.ProfileNav-item.ProfileNav-item--followers > a > span.ProfileNav-value")

    g = instagram_hola_yadira.xpath("//script").text
    # url = open("http://www.reddit.com/.json").read
    f = (/followed_by/ =~ g)
    final = /follows/ =~ g[f..-1] 
    # inicio = f + 12
    @contenido = g[f+13,final-15]
    # @contenido = JSON.parse(url)["kind"]
  
  end

  def list_markers
  	@markers = Point.all

  	list = []
  	@markers.each do |m|
  		ui = [m.title.to_s, m.lat.to_f, m.long.to_f]
  		list <<  ui
  	end

  	list
  end
end
