class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.string :lat
      t.string :long
      t.string :title
      t.text :desc

      t.timestamps
    end
  end
end
