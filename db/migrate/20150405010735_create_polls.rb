class CreatePolls < ActiveRecord::Migration
  def change
    create_table :polls do |t|
      t.string :point

      t.timestamps
    end
  end
end
